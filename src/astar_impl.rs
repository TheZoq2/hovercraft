use std::sync::mpsc::Sender;
use nalgebra as na;

pub type Vec2 = na::Vector2<f32>;

use crate::astar;

fn x_wall(curr: &Vec2) -> bool {
    !(curr.x == 50. && curr.y.abs() < 50.)
}

// Dijkstras algorithm in euclidean space
pub fn dijkstra(
    start: Vec2,
    goal: Vec2,
    sender: &Sender<na::Vector2<f32>>
) -> Option<Vec<(Vec2, Vec2)>> {
    let params = astar::AstarParams {
        actions: vec![
            Vec2::new(0., 1.),
            Vec2::new(0., -1.),
            Vec2::new(-1., 0.),
            Vec2::new(1., 0.)
        ],
        payload: &mut (),
        updater: |curr, action, _| Some(curr + action),
        edge_cost: |_| 1.,
        heuristic: |_curr, _target| 0.,
        discretiser: |vec: &Vec2| na::Vector2::<i32>::new(vec.x as i32, vec.y as i32),
        in_constraints: x_wall,
        end_condition: |_, _| false,
        _0: std::marker::PhantomData{},
    };

    astar::a_star(start, goal, params, sender)
}

// Dijkstras algorithm in euclidean space
pub fn euclid_astar(
    start: Vec2,
    goal: Vec2,
    sender: &Sender<na::Vector2<f32>>
) -> Option<Vec<(Vec2, Vec2)>> {
    let params = astar::AstarParams {
        actions: vec![
            Vec2::new(0., 1.),
            Vec2::new(0., -1.),
            Vec2::new(-1., 0.),
            Vec2::new(1., 0.)
        ],
        payload: &mut (),
        updater: |curr, action, _| Some(curr + action),
        edge_cost: |_| 1.,
        heuristic: |curr: &Vec2, target: &Vec2| (target-curr).norm(),
        discretiser: |vec: &Vec2| na::Vector2::<i32>::new(vec.x as i32, vec.y as i32),
        in_constraints: x_wall,
        end_condition: |_, _| false,
        _0: std::marker::PhantomData{},
    };

    astar::a_star(start, goal, params, sender)
}
