use nalgebra as na;

use na::{Point2, Vector2};

#[derive(Clone)]
pub struct World(pub Vec<(Vector2<f32>, Vector2<f32>)>);

impl World {
    pub fn is_outside_obstacles(&self, point: Point2<f32>) -> bool {
        !self.0.iter()
            .any(|(ul, lr)| {
                // point.x > ul.x && point.y > ul.y && point.x < lr.x && point.y < lr.y
                point.y < ul.y && point.y > lr.y
                    && point.x > ul.x && point.x < lr.x
            })
    }
}
