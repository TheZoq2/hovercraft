use std::f32::consts::PI;

use nalgebra as na;

use nphysics2d::object::Body;
use nphysics2d::algebra::ForceType;

use serde::{Serialize, Deserialize};

use na::{Point2, Vector2};

pub const RUDDER_OFFSET: f32 = -25.;

fn veci_to_vecf(input: Vector2<i32>) -> Vector2<f32> {
    Vector2::new(input.x as f32, input.y as f32)
}
fn pointi_to_pointf(input: Point2<i32>) -> Point2<f32> {
    Point2::from(veci_to_vecf(input.coords))
}

pub fn rudder_start_local() -> na::Point2<f32> {
    na::Point2::new(RUDDER_OFFSET, 0.)
}
pub fn rudder_vector(angle: f32) -> Vector2<f32> {
    -Vector2::new(angle.cos(), angle.sin())
}

fn discretise_float(input: f32, step_size: f32) -> i32 {
    // ((input - input.rem_euclid(step_size)) / step_size) as i32
    (input / step_size).round() as i32
}
fn discretise_vector(input: Vector2<f32>, step_size: f32) -> Vector2<i32> {
    Vector2::new(
        discretise_float(input.x, step_size),
        discretise_float(input.y, step_size)
    )
}
fn discretise_point(input: Point2<f32>, step_size: f32) -> Point2<i32> {
    discretise_vector(input.coords, step_size).into()
}
// We can't represent a unit complex rotation in discrete form, so we'll use
// a vector. This requires normalization when unconverting.
fn discretise_rotation(input: na::Unit<na::Complex<f32>>, step_size: f32) -> Vector2<i32> {
    let input = Vector2::new(input.re, input.im);
    let result = discretise_vector(input, step_size);
    Vector2::new(result.x, result.y)
}

#[derive(Clone, Copy)]
pub struct DiscSteps {
    pub pos: f32,
    pub vel: f32,
    pub rot: f32,
    pub ang_vel: f32,

    pub vel_pre: fn(Vector2<f32>) -> Vector2<f32>,
    pub inv_vel_pre: fn(Vector2<f32>) -> Vector2<f32>,
    pub ang_vel_pre: fn(f32) -> f32,
    pub inv_ang_vel_pre: fn(f32) -> f32,
}

pub fn id<T>(x: T) -> T {
    x
}
pub fn sqrt(x: f32) -> f32 {
    x.sqrt()
}
pub fn square(x: f32) -> f32 {
    x*x
}

pub fn sqrt_vec(v: Vector2<f32>) -> Vector2<f32> {
    Vector2::new(sqrt(v.x), sqrt(v.y))
}
pub fn square_vec(v: Vector2<f32>) -> Vector2<f32> {
    Vector2::new(sqrt(v.x), sqrt(v.y))
}

pub const MOTION_PLANNING_STEPS: DiscSteps = DiscSteps {
    pos: 2.0,
    vel: 2.0,
    rot: 0.05,
    ang_vel: 0.2,

    vel_pre: id,
    inv_vel_pre: id,
    ang_vel_pre: id,
    inv_ang_vel_pre: id,
};
pub const LATTICE_PLANNING_STEPS: DiscSteps = DiscSteps {
    pos: 16.0,
    vel: 8.0,
    rot: 0.2,
    ang_vel: 1.,

    vel_pre: sqrt_vec,
    inv_vel_pre: square_vec,
    ang_vel_pre: sqrt,
    inv_ang_vel_pre: square
};


#[derive(Clone, PartialEq, Eq, Hash)]
pub struct DiscreteState {
    pub velocity: Vector2<i32>,
    pub position: Point2<i32>,
    pub rotation: Vector2<i32>,
    pub angular_velocity: i32,
}

impl DiscreteState {
    pub fn as_state(&self, steps: DiscSteps) -> State {
        let position = pointi_to_pointf(self.position) * steps.pos;
        let rotation_vec = veci_to_vecf(self.rotation) * steps.rot;
        let rotation = na::Unit::new_normalize(
            na::Complex::new(rotation_vec.x, rotation_vec.y)
        );
        State {
            velocity: (steps.inv_vel_pre)(veci_to_vecf(self.velocity) * steps.vel),
            position,
            rotation,
            angular_velocity: (steps.inv_ang_vel_pre)(self.angular_velocity as f32 * steps.ang_vel),
        }
    }
}


#[derive(Clone, Debug, Copy, Deserialize, Serialize, PartialEq)]
pub struct State {
    pub velocity: Vector2<f32>,
    pub position: Point2<f32>,
    pub rotation: na::Unit<na::Complex<f32>>,
    pub angular_velocity: f32,
}

impl State {
    pub fn initial() -> Self {
        Self {
            position: na::Point2::new(0., 0.),
            velocity: na::Vector2::new(0., 0.),
            rotation: na::Unit::new_normalize(na::Complex::new(1., 0.)),
            angular_velocity: 0.0,
        }
    }

    pub fn from_body(body: &dyn Body<f32>) -> Self {
        let part = body.part(0).unwrap();
        let transform = part.position();
        let velocity = part.velocity();

        Self {
            position: transform.translation.vector.into(),
            rotation: transform.rotation,
            angular_velocity: velocity.angular,
            velocity: velocity.linear,
        }
    }

    pub fn make_discrete(&self, steps: DiscSteps) -> DiscreteState {
        DiscreteState {
            velocity: discretise_vector((steps.vel_pre)(self.velocity), steps.vel),
            position: discretise_point(self.position, steps.pos),
            rotation: discretise_rotation(self.rotation, steps.rot),
            angular_velocity: discretise_float(
                (steps.ang_vel_pre)(self.angular_velocity),
                steps.ang_vel
            ),
        }
    }

    pub fn get_transform(&self) -> na::Isometry2<f32> {
        na::Isometry2::from_parts(
            na::Translation2{vector: self.position.coords},
            self.rotation
        )
    }

    pub fn then(&self, next: &Self) -> Self {
        let rotation = na::Rotation2::new(self.rotation.angle());
        let rotated_velocity = rotation * next.velocity;
        let rotated_position = rotation * next.position;
        Self {
            velocity: self.velocity + rotated_velocity, // next.velocity,
            position: self.position + rotated_position.coords,
            rotation: self.rotation * next.rotation,
            angular_velocity: self.angular_velocity + next.angular_velocity
        }
    }
}



#[derive(Clone, Debug, Copy, Deserialize, Serialize)]
pub struct Input {
    pub rudder_angle: f32,
    pub thrust: f32,
}

impl Input {
    pub fn new(rudder_angle: f32, thrust: f32) -> Self {
        Self {
            rudder_angle,
            thrust
        }
    }
    pub fn all_inputs() -> Vec<Self> {
        let max_rudder_angle = PI/8.;
        let max_thrust = 10.;
        vec![
            Self::new(0., 0.),
            Self::new(-max_rudder_angle, max_thrust),
            Self::new(max_rudder_angle, max_thrust),
            Self::new(0., max_thrust),
        ]
    }

    pub fn apply(&self, body: &mut dyn Body<f32>) {
        body.apply_local_force_at_local_point(
            0,
            &(-rudder_vector(self.rudder_angle) * self.thrust),
            &rudder_start_local(),
            ForceType::Force,
            true
        );
    }
}
