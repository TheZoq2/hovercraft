use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::mpsc::Sender;

use ordered_float::OrderedFloat;

use log::{info, warn};

pub struct Node<T, A> {
    state: T,
    actions: Vec<A>,
    cost: f32,
    heuristic: f32,
}

impl<T, A> Node<T, A> {
    fn heuristic_cost(&self) -> OrderedFloat<f32> {
        (self.cost + self.heuristic).into()
    }
}

impl<T, A> PartialEq for Node<T, A> {
    fn eq(&self, other: &Self) -> bool {
        self.heuristic_cost().eq(&other.heuristic_cost())
    }
}
impl<T, A> Eq for Node<T, A> {}

// We want as few steps as possible, so we'll invert the ordering
impl<T, A> PartialOrd for Node<T, A> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        other.heuristic_cost().partial_cmp(&self.heuristic_cost())
    }
}
impl<T, A> Ord for Node<T, A> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.heuristic_cost().cmp(&self.heuristic_cost())
    }
}

pub struct AstarParams<
    'a,
    T,
    A,
    DiscreteT,
    Payload,
    Updater: Fn(&T, &A, &mut Payload) -> Option<T>,
    EdgeCost: Fn(&A) -> f32,
    Heuristic: Fn(&T, &T) -> f32,
    Discretiser: Fn(&T) -> DiscreteT,
    Constraints: Fn(&T) -> bool,
    EndCondition: Fn(&T, &T) -> bool,
> where
    DiscreteT: Eq + std::hash::Hash,
{
    pub actions: Vec<A>,
    pub payload: &'a mut Payload,
    /// (state, action, payload)
    /// Perform a state transition by applying the `action` in the `state`. The returned value is
    /// the new state. The payload is extra data that can be used to perform the update, like a
    /// physics world
    pub updater: Updater,
    /// (from, to)
    /// Compute the cost of going from `from` to `to`
    pub edge_cost: EdgeCost,
    /// (state, target)
    /// Heuristic for how close `state` is to `target`. Must be admissible
    /// for the optimal path to be found.
    pub heuristic: Heuristic,
    /// Compute a discrete version of T. This defines the grid size that the search will take place
    /// in. If two `T`s produce the same `DiscreteT`, then only the one with the lower cost will be
    /// considered in the search.
    pub discretiser: Discretiser,
    /// Constraints on the search space. Return `true` if the T is a valid point.
    pub in_constraints: Constraints,
    /// (state, target)
    /// A condition for ending the search that is not an exact state. This could,
    /// for example, be a condition on velocity and angle, but not position and
    /// angular velocity
    pub end_condition: EndCondition,
    pub _0: std::marker::PhantomData<(T, A, DiscreteT)>,
}

pub fn a_star<
    'a,
    T,
    A,
    DiscreteT,
    Payload,
    Updater: Fn(&T, &A, &mut Payload) -> Option<T>,
    EdgeCost: Fn(&A) -> f32,
    Heuristic: Fn(&T, &T) -> f32,
    Discretiser: Fn(&T) -> DiscreteT,
    Constraints: Fn(&T) -> bool,
    EndCondition: Fn(&T, &T) -> bool,
>(
    start: T,
    target: T,
    p: AstarParams<
        'a,
        T,
        A,
        DiscreteT,
        Payload,
        Updater,
        EdgeCost,
        Heuristic,
        Discretiser,
        Constraints,
        EndCondition,
    >,
    visited_sender: &Sender<T>,
) -> Option<Vec<(T, A)>>
where
    T: Clone,
    A: Clone,
    DiscreteT: Eq + std::hash::Hash + Clone,
    T: std::fmt::Debug,
{
    let mut candidates = BinaryHeap::new();
    let mut visited = HashSet::new();
    let mut best_steps = HashMap::new();

    // Set up the start node
    best_steps.insert((p.discretiser)(&start), 0.);
    let start_node = Node {
        heuristic: (p.heuristic)(&start, &target),
        state: start,
        actions: vec![],
        cost: 0.,
    };
    candidates.push(start_node);

    // Diagnostic stuff
    let mut last_report = std::time::SystemTime::now();
    let mut search_count = 0;


    while let Some(candidate) = candidates.pop() {
        // Check if we just popped the target
        // if (p.discretiser)(&candidate.state) == (p.discretiser)(&target) {
        //     info!("Found exact target");
        //     return Some(candidate.actions);
        // }
        if (p.end_condition)(&candidate.state, &target) {
            info!("Found approximate target");
            info!("End state: {:#?}", candidate.state);
            return Some(candidate.actions);
        }

        let discrete = (p.discretiser)(&candidate.state);
        if candidate.cost > best_steps[&discrete] {
            continue;
        }

        visited.insert(discrete.clone());
        // visited_sender.send(undiscretiser(discrete)).expect("remote end hung up");

        // Set up the physics world with the current state
        for action in &p.actions {
            let update_result = (p.updater)(&candidate.state, &action, p.payload);
            let new_state = if let Some(state) = update_result {
                state
            }
            else {
                continue;
            };

            let cost = candidate.cost + (p.edge_cost)(&action);
            let discrete = (p.discretiser)(&new_state);

            let not_visited = !visited.contains(&(p.discretiser)(&new_state));
            let prev_cost = best_steps.get(&discrete).unwrap_or(&std::f32::MAX);
            let is_best_cost = cost < *prev_cost;

            if not_visited && is_best_cost && (p.in_constraints)(&new_state) {
                let mut new_actions = candidate.actions.clone();
                new_actions.push((new_state.clone(), action.clone()));

                best_steps.insert(discrete.clone(), cost);
                visited_sender.send(new_state.clone()).unwrap();

                let new_node = Node {
                    cost,
                    state: new_state,
                    actions: new_actions,
                    heuristic: (p.heuristic)(&candidate.state, &target)
                };

                candidates.push(new_node);
            }
        }

        search_count += 1;
        if last_report.elapsed().unwrap() > std::time::Duration::from_secs(1) {
            info!("Processed {} states per second", search_count);
            search_count = 0;
            last_report = std::time::SystemTime::now();
        }
        // std::thread::sleep(std::time::Duration::from_micros(50000));
    }
    warn!("visited size: {}", visited.len());
    None
}




