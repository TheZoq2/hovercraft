use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::f32::consts::PI;
use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};

use ordered_float::OrderedFloat;

use crate::modelling::{
    DiscreteState,
    Input,
    State,
    MOTION_PLANNING_STEPS,
};
use crate::{astar::AstarParams, physics::Physics};

use log::{info, warn};

// Computes a set of inputs that takes the hovercraft from one state to the next.  The partials
// argument is for sharing the current progress with some rendering thread
pub fn plan_motion_primitive(
    start: State,
    target: State,
    end_condition: impl Fn(&State, &State) -> bool,
    physics: &mut Physics,
    // partials: Arc<Mutex<BinaryHeap<Vec<(Input, State)>>>>,
    visited_sender: &Sender<State>,
) -> Option<Vec<(State, Input)>> {
    let steps = MOTION_PLANNING_STEPS;
    let in_constraints = |state: &State| {
        state.velocity.norm() < 30. && state.angular_velocity < 10.
    };

    assert!(in_constraints(&target));

    let params = AstarParams {
        actions: vec![
            Input {
                rudder_angle: 0.,
                thrust: 10.,
            },
            Input {
                rudder_angle: PI / 4.,
                thrust: 10.,
            },
            Input {
                rudder_angle: -PI / 4.,
                thrust: 10.,
            },
            Input {
                rudder_angle: 0.,
                thrust: 0.,
            },
        ],
        payload: physics,
        updater: |state: &State, action: &Input, physics: &mut Physics| {
            physics.replace_hovercraft(state);
            physics.step_with_input(action);
            physics.step_with_input(action);
            Some(physics.hovercraft_state())
        },
        edge_cost: |_| 1.,
        heuristic: |curr, dest| {
            let target_dir = curr.position - dest.position;

            // Penalise being far from the target
            let position_penalty = (curr.position - dest.position).norm();
            // Penalise an incorrect velocity, there is not enough time to break
            // if this is reached
            let velocity_penalty = ((curr.velocity) - (dest.velocity)).norm();
            // Give a hard penalty to going faster than the goal
            let velocity_high_penalty =
                if curr.velocity.norm() > dest.velocity.norm() {
                    1.
                } else {
                    0.
                };
            // Penalise being off angle
            let rotation_penalty =
                (dest.rotation.angle_to(&curr.rotation)).abs();

            // // (position_penalty / 100.
            //     ( velocity_penalty
            //     // + velocity_high_penalty * 1000.
            //     + rotation_penalty)
            //     * 100.
            0.
            // curr.velocity.norm()
            // curr.rotation.angle_to(&target.rotation).abs()
        },
        discretiser: |state| state.make_discrete(steps),
        in_constraints,
        end_condition,
        _0: std::marker::PhantomData {},
    };

    crate::astar::a_star(
        start,
        target,
        params,
        visited_sender
    )
}
