use nalgebra as na;

use na::Vector2;
use nphysics2d::force_generator::DefaultForceGeneratorSet;
use nphysics2d::joint::DefaultJointConstraintSet;
use nphysics2d::object::{
    DefaultBodyHandle, DefaultBodySet, DefaultColliderSet, RigidBody, RigidBodyDesc,
};
use nphysics2d::world::{DefaultGeometricalWorld, DefaultMechanicalWorld};
use nphysics2d::algebra::Velocity2;

use crate::modelling::{State, Input};

pub struct Physics {
    world: DefaultMechanicalWorld<f32>,
    geometrical_world: DefaultGeometricalWorld<f32>,
    bodies: DefaultBodySet<f32>,
    colliders: DefaultColliderSet<f32>,
    joint_constraints: DefaultJointConstraintSet<f32>,
    force_generators: DefaultForceGeneratorSet<f32>,

    hovercraft_handle: DefaultBodyHandle,
    secondary_handle: DefaultBodyHandle,
}

fn base_hovercraft() -> RigidBodyDesc<f32> {
    RigidBodyDesc::new()
        .rotation(0.)
        .mass(0.1)
        .angular_inertia(30.)
}

impl Physics {
    pub fn new() -> Self {
        let body = base_hovercraft().build();

        let mut bodies = DefaultBodySet::new();
        let hovercraft_handle = bodies.insert(body);
        let secondary_handle = bodies.insert(base_hovercraft().build());

        Self {
            world: DefaultMechanicalWorld::new(Vector2::new(0.0, 0.)),
            geometrical_world: DefaultGeometricalWorld::new(),
            bodies,
            colliders: DefaultColliderSet::new(),
            joint_constraints: DefaultJointConstraintSet::new(),
            force_generators: DefaultForceGeneratorSet::new(),
            hovercraft_handle,
            secondary_handle,
        }
    }

    // Applies an input to the hovercraft then steps the physics simulation
    pub fn step_with_input(&mut self, input: &Input) {
        let hovercraft = self.bodies.get_mut(self.hovercraft_handle).unwrap();
        input.apply(hovercraft);

        // Do physics update


        // Update the physics
        self.world.step(
            &mut self.geometrical_world,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joint_constraints,
            &mut self.force_generators,
        );
    }

    pub fn hovercraft_state(&self) -> State {
        let hovercraft = self.bodies.get(self.hovercraft_handle).unwrap();

        State::from_body(hovercraft)
    }

    pub fn replace_hovercraft(&mut self, state: &State) {
        self.bodies.remove(self.hovercraft_handle);

        let new = base_hovercraft()
            .translation(state.position.coords)
            .rotation(state.rotation.angle())
            .velocity(Velocity2::new(state.velocity, state.angular_velocity))
            .build();

        self.hovercraft_handle = self.bodies.insert(new)
    }

    pub fn replace_secondary(&mut self, state: &State) {
        self.bodies.remove(self.secondary_handle);

        let new = base_hovercraft()
            .translation(state.position.coords)
            .rotation(state.rotation.angle())
            .velocity(Velocity2::new(state.velocity, state.angular_velocity))
            .build();

        self.hovercraft_handle = self.bodies.insert(new)
    }

    // Applies an input to the hovercraft then steps the physics simulation
    pub fn set_secondary_input(&mut self, input: &Input) {
        let hovercraft = self.bodies.get_mut(self.hovercraft_handle).unwrap();
        input.apply(hovercraft);
    }
}
