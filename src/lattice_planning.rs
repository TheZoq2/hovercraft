use std::fs::File;
use std::io::prelude::*;
use std::sync::mpsc::Sender;

use crate::astar::AstarParams;
use crate::modelling::{Input, State, LATTICE_PLANNING_STEPS};
use crate::physics::Physics;
use crate::world::World;

use log::info;

pub fn do_lattice_planning(
    from: State,
    to: State,
    world: World,
    visited_sender: Sender<State>,
    input_tx: Sender<Vec<Input>>,
    debug_state_sender: Sender<State>,
) {
    std::thread::spawn(move || {
        // Load the motion primitives
        let mut file = File::open("primitives.json")
            .expect("Failed to open motion primitives");
        let mut content = String::new();
        file.read_to_string(&mut content)
            .expect("failed to read motion primitives");

        let primitives = serde_json::from_str::<Vec<Vec<(State, Input)>>>(&content)
            .expect("Failed to decode primitives");

        // for primitive in &primitives {
        //     visited_sender.send(primitive[primitive.len()-1].0);
        // }

        // loop {}
        let steps = LATTICE_PLANNING_STEPS;

        let in_constraints = |s: &State| {
            true
            // && !((s.position.y + 25.).abs() < 10. && (s.position.x.abs() < 50.))
            && world.is_outside_obstacles(s.position)
            && s.velocity.norm() < 30. && s.angular_velocity.abs() < 10.
        };

        let params = AstarParams {
            actions: primitives,
            payload: &mut Physics::new(),
            updater: |state: &State, input_seq, physics| {
                physics.replace_hovercraft(state);
                for (_state, input) in input_seq {
                    physics.step_with_input(input);
                    if !in_constraints(&physics.hovercraft_state()) {
                        return None
                    }
                }
                let from_sim = physics.hovercraft_state();
                // println!("Trying a state");
                // let from_seq = state.then(&input_seq[input_seq.len() - 1].0);
                // assert_eq!(from_sim, from_seq);
                Some(from_sim)
            },
            edge_cost: |action| action.len() as f32,
            heuristic: |s, t| {
                // 0.
                (s.position-t.position).norm()
                    // + s.velocity.norm() * 100.
            },
            discretiser: |state| state.make_discrete(steps),
            in_constraints,
            end_condition: |s, t| {
                s.make_discrete(steps).position == t.make_discrete(steps).position
                && s.make_discrete(steps).velocity == t.make_discrete(steps).velocity
                && s.make_discrete(steps).angular_velocity == t.make_discrete(steps).angular_velocity
            },
            _0: std::marker::PhantomData{},
        };

        let result = crate::astar::a_star(from, to, params, &visited_sender).unwrap();

        println!("{}", result.len());

        // Create a new physics simulation to verrify the result
        let mut physics = Physics::new();
        physics.replace_hovercraft(&from);
        println!("{:?}", from);

        // Resulting input sequence
        let mut input_seq: Vec<Input> = vec![];

        // Go through each action, extracting the input sequence
        for (_state, action) in &result {
            let mut end_state;
            for (_state, input) in action {
                // Simulate the input
                physics.step_with_input(&input);
                end_state = physics.hovercraft_state();

                // std::thread::sleep(std::time::Duration::from_millis(2000));
                debug_state_sender.send(end_state).unwrap();
                // Store this input in the result vector
                input_seq.push(input.clone())
            }
        }
        info!("Lattice planning done");
        input_tx.send(input_seq).unwrap();
    });
}
