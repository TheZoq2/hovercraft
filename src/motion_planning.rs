use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};
use std::fs::File;
use std::io::prelude::*;

use nalgebra as na;

use serde_json::ser;

use crate::modelling::{State, Input, DiscreteState, MOTION_PLANNING_STEPS};
use crate::physics;
use crate::planning;

use log::{warn, error, info};

fn focus_angle(state: &State, target_state: &State) -> bool {
    let discrete = state.make_discrete(MOTION_PLANNING_STEPS);
    let target = target_state.make_discrete(MOTION_PLANNING_STEPS);
    // discrete.velocity == target.velocity
    true
        && target.rotation == discrete.rotation
        && target.angular_velocity == discrete.angular_velocity
}
fn focus_velocity(state: &State, target_state: &State) -> bool {
    let discrete = state.make_discrete(MOTION_PLANNING_STEPS);
    let target = target_state.make_discrete(MOTION_PLANNING_STEPS);
    // discrete.velocity == target.velocity
    true
        && target.velocity == discrete.velocity
        && target.angular_velocity == discrete.angular_velocity
}
fn focus_angular_velocity(state: &State, target_state: &State) -> bool {
    let discrete = state.make_discrete(MOTION_PLANNING_STEPS);
    let target = target_state.make_discrete(MOTION_PLANNING_STEPS);

    // discrete.velocity == target.velocity
    true
        && target.angular_velocity == discrete.angular_velocity
        // && target.rotation == discrete.rotation
}


pub fn do_motion_planning(
    tx: Sender<State>,
    output_file: std::path::PathBuf,
    input_tx: Sender<Vec<Input>>,
    input_sequence_done: Arc<Mutex<bool>>,
) {
    std::thread::spawn(move || {
        let mut motion_primitives = vec![];
        let starting_state = State {
            position: na::Point2::new(0., 0.),
            velocity: na::Vector2::new(0., 0.),
            rotation: na::Unit::new_normalize(na::Complex::new(1., 0.)),
            angular_velocity: 0.,
        };

        let specific_angle = |rotation| {
            State {
                position: na::Point2::new(45., -30.),
                velocity: na::Vector2::new(0., 0.),
                rotation,
                angular_velocity: 0.,
            }
        };

        let specific_velocity = |velocity| {
            State {
                position: na::Point2::new(0., 0.),
                velocity,
                rotation: na::Unit::new_normalize(na::Complex::new(1., 0.)),
                angular_velocity: 0.,
            }
        };

        let specific_angular_velocity = |angular_velocity| {
            State {
                position: na::Point2::new(0., 0.),
                velocity: na::Vector2::new(0., 0.),
                rotation: na::Unit::new_normalize(na::Complex::new(1., 0.)),
                angular_velocity,
            }
        };

        let targets: Vec<(_, _, &dyn Fn(&State, &State) -> bool)> = vec! [
            // Pure rotations
            (
                "Rotation",
                specific_angle(na::Unit::new_normalize(na::Complex::new(0., 1.))),
                &focus_angle
            ),
            (
                "Rotation",
                specific_angle(na::Unit::new_normalize(na::Complex::new(1., 1.))),
                &focus_angle
            ),
            (
                "Rotation",
                specific_angle(na::Unit::new_normalize(na::Complex::new(1., -1.))),
                &focus_angle
            ),
            (
                "Rotation",
                specific_angle(na::Unit::new_normalize(na::Complex::new(0., -1.))),
                &focus_angle
            ),
            // (
            //     "Rotation",
            //     specific_angle(na::Unit::new_normalize(na::Complex::new(-1., -1.))),
            //     &focus_angle
            // ),
            // (
            //     "Rotation",
            //     specific_angle(na::Unit::new_normalize(na::Complex::new(-1., 1.))),
            //     &focus_angle
            // ),

            // Velocity changes
            (
                "Velocity",
                specific_velocity(na::Vector2::new(15., 0.)),
                &focus_velocity
            ),
            (
                "Velocity",
                specific_velocity(na::Vector2::new(20., 0.)),
                &focus_velocity
            ),
            (
                "Velocity",
                specific_velocity(na::Vector2::new(25., 0.)),
                &focus_velocity
            ),
            // Angular velocity changes
            (
                "Angular velocity",
                specific_angular_velocity(1.),
                &focus_angular_velocity,
            ),
            (
                "Angular velocity",
                specific_angular_velocity(2.),
                &focus_angular_velocity,
            ),
            (
                "Angular velocity",
                specific_angular_velocity(-1.),
                &focus_angular_velocity,
            ),
            (
                "Angular velocity",
                specific_angular_velocity(-2.),
                &focus_angular_velocity,
            ),
        ];

        let mut physics = physics::Physics::new();
        for (i, (d, target, condition)) in targets.into_iter().enumerate() {
            info!("Searching for {}", d);
            let result = planning::plan_motion_primitive(
                starting_state,
                // State {
                //     position: na::Point2::new(100., 0.),
                //     velocity: na::Vector2::new(0., 0.),
                //     rotation: na::Unit::new_normalize(na::Complex::new(-1., 0.)),
                //     angular_velocity: 0.
                // },
                target,
                condition,
                &mut physics,
                &tx,
            );

            warn!("result {} is found: {}", i, result.is_some());

            if let Some(result) = result {
                let mut multiplied = vec![];
                // Since the dijkstra thingy is using n steps per input,
                // we have to do so as well
                for state_input in result {
                    multiplied.push(state_input);
                    multiplied.push(state_input);
                }

                // Store this sequence
                motion_primitives.push(multiplied.clone());

                // Pass it onto the rendering thread
                let inputs = multiplied.into_iter()
                    .map(|(_, input)| input)
                    .collect();
                input_tx.send(inputs).unwrap();
            }
            else {
                error!("No input sequence found");
            }

            while *input_sequence_done.lock().unwrap() == false {
            }
            *input_sequence_done.lock().unwrap() = true;
            std::thread::sleep_ms(1000);
        }

        let null_action = (starting_state, Input{rudder_angle: 0., thrust: 0.});
        motion_primitives.push(vec![null_action; 6]);
        motion_primitives.push(vec![null_action; 16]);
        motion_primitives.push(vec![null_action; 25]);
        motion_primitives.push(vec![null_action; 60]);

        for seq in &motion_primitives {
            println!("length of seq: {}", seq.len());
        }

        let mut outfile = File::create(&output_file)
            .expect(&format!("Failed to open: {:?}", output_file));
        let encoded = &ser::to_string(&motion_primitives)
            .expect("Failed to encode motion primitives");
        outfile.write_all(&encoded.as_bytes()).expect("Failed to write output")
    });
}
