use nalgebra as na;

use std::collections::VecDeque;
use std::f32::consts::PI;
use std::ffi::CString;
use std::sync::mpsc::{channel, Receiver};
use std::sync::{Arc, Mutex};
use std::path::PathBuf;
use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

use na::Vector2;
use nphysics2d::force_generator::DefaultForceGeneratorSet;
use nphysics2d::joint::DefaultJointConstraintSet;
use nphysics2d::object::{
    DefaultBodyHandle, DefaultBodySet, DefaultColliderSet, RigidBody, RigidBodyDesc,
};
use nphysics2d::world::{DefaultGeometricalWorld, DefaultMechanicalWorld};

use log::{error, warn, info};

use skulpin::skia_safe;
use skulpin::CoordinateSystem;
use skulpin::LogicalSize;

use skia_safe::Canvas;
use skulpin::app::AppBuilder;
use skulpin::app::AppDrawArgs;
use skulpin::app::AppError;
use skulpin::app::AppHandler;
use skulpin::app::AppUpdateArgs;
use skulpin::app::VirtualKeyCode;

mod world;
mod astar;
mod astar_impl;
mod modelling;
mod physics;
mod planning;
mod motion_planning;
mod lattice_planning;

use modelling::{rudder_start_local, rudder_vector, DiscreteState, Input, State};

fn p(point: Vector2<f32>) -> skia_safe::Point {
    skia_safe::Point::new(point.x, point.y)
}

fn main() {
    // Setup logging
    env_logger::Builder::from_default_env()
        .filter_level(log::LevelFilter::Debug)
        .filter(Some("skulpin_renderer::"), log::LevelFilter::Warn)
        .init();

    let (debug_tx, debug_rx) = channel();
    let (input_tx, input_rx) = channel();
    let (tx, rx) = channel();

    let target_state = State {
        position: na::Point2::new(45., -30.),
        velocity: na::Vector2::new(0., 0.),
        rotation: na::Unit::new_normalize(na::Complex::new(1., 1.)),
        angular_velocity: 0.,
    };

    let input_sequence_done = Arc::new(Mutex::new(false));

    let world = world::World(vec![
        (Vector2::new(-50., -45.), Vector2::new(50., -55.)),
        (Vector2::new(45., 50.), Vector2::new(55., -50.))
    ]);

    let example_app = ExampleApp::new(
        rx,
        debug_rx,
        target_state.clone(),
        input_rx,
        input_sequence_done.clone(),
        world.clone(),
    );


    // Set up the coordinate system to be fixed at 900x600, and use this as the default window size
    // This means the drawing code can be written as though the window is always 900x600. The
    // output will be automatically scaled so that it's always visible.
    let logical_size = LogicalSize::new(900, 600);
    let scale = 2.;
    let visible_range = skulpin::skia_safe::Rect {
        left: -(logical_size.width as f32) / scale / 2.,
        right: logical_size.width as f32 / scale / 2.,
        top: logical_size.height as f32 / scale / 2.,
        bottom: -(logical_size.height as f32) / scale / 2.,
    };
    let scale_to_fit = skulpin::skia_safe::matrix::ScaleToFit::Center;

    // motion_planning::do_motion_planning(
    //     tx,
    //     PathBuf::from("primitives.json"),
    //     input_tx,
    //     input_sequence_done
    // );
    lattice_planning::do_lattice_planning(
        State::initial(),
        State {
            position: na::Point2::new(0., -100.),
            velocity: na::Vector2::new(0., 0.),
            rotation: na::Unit::new_normalize(na::Complex::new(0., -1.)),
            angular_velocity: 0.0,
        },
        world.clone(),
        tx,
        input_tx,
        debug_tx,
    );

    // std::thread::spawn(move || {
    //     let result = astar_impl::euclid_astar(
    //         Vector2::new(0., 0.),
    //         Vector2::new(100., 0.),
    //         &v_tx,
    //     );
    //     warn!("Dijkstra is done: {}", result.is_some());
    // });

    AppBuilder::new()
        .app_name(CString::new("Skulpin Example App").unwrap())
        .use_vulkan_debug_layer(false)
        .inner_size(logical_size)
        .coordinate_system(CoordinateSystem::VisibleRange(
            visible_range,
            scale_to_fit,
        ))
        .run(example_app);
}

struct ExampleApp {
    physics: physics::Physics,
    last_input: Input,
    last_states: VecDeque<State>,
    target: State,
    // Debug for motion planner
    visited_states: VecDeque<State>,
    visited_receiver: Receiver<State>,

    debug_states: Vec<State>,
    debug_state_receiver: Receiver<State>,

    input_rx: Receiver<Vec<Input>>,
    input_queue: VecDeque<Input>,

    input_sequence_done: Arc<Mutex<bool>>,

    reached_states: Vec<State>,

    visited_positions: HashMap<na::Vector2<i32>, usize>,

    prev_state: State,

    world: world::World,
}

impl ExampleApp {
    pub fn new(
        visited_receiver: Receiver<State>,
        debug_state_receiver: Receiver<State>,
        target: State,
        input_rx: Receiver<Vec<Input>>,
        input_sequence_done: Arc<Mutex<bool>>,
        world: world::World,
    ) -> Self {
        let state = State::initial();

        // Load the motion primitives
        let mut file = File::open("primitives.json")
            .expect("Failed to open motion primitives");
        let mut content = String::new();
        file.read_to_string(&mut content)
            .expect("failed to read motion primitives");

        let primitives = serde_json::from_str::<Vec<Vec<(State, Input)>>>(&content)
            .expect("Failed to decode primitives");
        let reached_states = primitives.into_iter()
            .map(|x| x[x.len() - 1].0)
            .collect();

        ExampleApp {
            physics: physics::Physics::new(),
            last_input: Input::new(0., 0.),
            last_states: VecDeque::new(),
            visited_states: VecDeque::new(),
            visited_receiver,
            target,
            debug_state_receiver,
            debug_states: vec![],
            input_rx,
            input_queue: VecDeque::new(),
            input_sequence_done,
            reached_states,
            visited_positions: HashMap::new(),
            prev_state: state,
            world,
        }
    }
}

impl AppHandler for ExampleApp {
    fn update(&mut self, update_args: AppUpdateArgs) {
        for input in self.input_rx.try_iter() {
            info!("Got input");
            self.physics = physics::Physics::new();
            let initial = State::initial();
            self.physics.replace_hovercraft(&initial);
            self.prev_state = initial;

            self.input_queue = input.into();
        }
        let input_state = update_args.input_state;
        let app_control = update_args.app_control;

        if input_state.is_key_down(VirtualKeyCode::Escape) {
            app_control.enqueue_terminate_process();
        }

        let rudder_angle = if input_state.is_key_down(VirtualKeyCode::D) {
            PI / 6.
        } else if input_state.is_key_down(VirtualKeyCode::A) {
            -PI / 6.
        } else {
            0.
        };

        let thrust = if input_state.is_key_down(VirtualKeyCode::W) {
            9.82
        } else {
            0.
        };

        self.last_input = if let Some(input) = self.input_queue.pop_front() {
            if self.input_queue.is_empty() {
                *self.input_sequence_done.lock().unwrap() = true;
                warn!("Done running input sequence");
                self.reached_states.push(self.physics.hovercraft_state())
            }
            input
        }
        else {
            Input::new(rudder_angle, thrust)
        };
        // self.last_input = Input::new(rudder_angle, thrust);

        // self.physics.replace_hovercraft(&self.prev_state);
        self.physics.step_with_input(&self.last_input);
        self.prev_state = self.physics.hovercraft_state();

        let resulting_state = self.physics.hovercraft_state();
        self.last_states.push_back(resulting_state);
        if self.last_states.len() > 240 {
            self.last_states.pop_front();
        }

        // println!(
        //     "angular_velocity: {}, {}",
        //     resulting_state.angular_velocity,
        //     resulting_state.make_discrete().angular_velocity,
        // );

        for state in self.visited_receiver.try_iter() {
            let pos = na::Vector2::new(
                state.position.x as i32,
                state.position.y as i32
            );
            *self.visited_positions.entry(pos).or_insert(0) += 1;
            self.visited_states.push_back(state);
            if self.visited_states.len() > 1000 {
                self.visited_states.pop_front();
            }
        }

        for vec in self.debug_state_receiver.try_iter() {
            self.debug_states.push(vec);
        }
    }

    fn draw(&mut self, draw_args: AppDrawArgs) {
        let canvas = draw_args.canvas;

        // Generally would want to clear data every time we draw
        canvas.clear(skia_safe::Color::from_argb(0, 0, 0, 255));

        for state in &self.visited_states {
            draw_state(&state, canvas, search_colors());
        }
        draw_state(
            &self.target,
            canvas,
            target_colors(),
        );

        // for state in &self.reached_states {
        //     draw_state(&state, canvas, target_colors());
        // }

        let mut paint =
            skia_safe::Paint::new(skia_safe::Color4f::new(1.0, 1.0, 1., 1.0), None);
        paint.set_anti_alias(true);
        paint.set_style(skia_safe::paint::Style::Stroke);
        paint.set_stroke_width(1.0);
        for state in &self.debug_states {
            draw_state(&state, canvas, target_colors());
        }

        let mut max = 0;
        for (pos, count) in &self.visited_positions {
            max = max.max(*count);
            let mut paint =
                skia_safe::Paint::new(skia_safe::Color4f::new(
                    1.0,
                    1.0,
                    1.,
                    *count as f32 / 1000.
                ), None);
            canvas.draw_circle(
                skia_safe::Point::new(pos.x as f32, pos.y as f32),
                4.,
                &paint
            );
        }
        // println!("{}", max)

        draw_craft(&self.physics.hovercraft_state(), &self.last_input, canvas, red());

        for (ul, lr) in &self.world.0 {
            let ur = skia_safe::Point::new(lr.x, ul.y);
            let ll = skia_safe::Point::new(ul.x, lr.y);
            let ul = skia_safe::Point::new(ul.x, ul.y);
            let lr = skia_safe::Point::new(lr.x, lr.y);
            canvas.draw_line(ul, ur, &white());
            canvas.draw_line(ur, lr, &white());
            canvas.draw_line(lr, ll, &white());
            canvas.draw_line(ll, ul, &white());
        }
    }

    fn fatal_error(&mut self, error: &AppError) {
        println!("{}", error);
    }
}

pub fn draw_craft(
    state: &State,
    input: &Input,
    canvas: &mut Canvas,
    paint: skia_safe::Paint
) {
    let direction = na::Vector2::new(state.rotation.re, state.rotation.im);

    canvas.draw_circle(p(state.position.coords), 25.0, &paint);

    canvas.draw_line(
        p(state.position.coords),
        p(state.position.coords + direction * 35.),
        &paint,
    );

    let rudder_end = na::Point2::from(
        rudder_start_local() + rudder_vector(input.rudder_angle) * 20.,
    );

    canvas.draw_line(
        p(state
            .get_transform()
            .transform_point(&rudder_start_local())
            .coords),
        p(state.get_transform().transform_point(&rudder_end).coords),
        &paint,
    );
}

fn white() -> skia_safe::Paint {
    let mut result =
        skia_safe::Paint::new(skia_safe::Color4f::new(1.0, 1.0, 1., 1.0), None);
    result.set_anti_alias(true);
    result.set_style(skia_safe::paint::Style::Stroke);
    result.set_stroke_width(1.0);
    result
}
fn red() -> skia_safe::Paint {
    let mut result =
        skia_safe::Paint::new(skia_safe::Color4f::new(1.0, 0.0, 0., 1.0), None);
    result.set_anti_alias(true);
    result.set_style(skia_safe::paint::Style::Stroke);
    result.set_stroke_width(1.0);
    result
}

struct StateColors {
    position: skia_safe::Paint,
    velocity: skia_safe::Paint,
}

fn search_colors() -> StateColors {
    let mut position =
        skia_safe::Paint::new(skia_safe::Color4f::new(1.0, 0.0, 1., 0.2), None);
    position.set_anti_alias(true);
    position.set_style(skia_safe::paint::Style::Stroke);
    position.set_stroke_width(1.0);

    let mut velocity =
        skia_safe::Paint::new(skia_safe::Color4f::new(0.0, 1.0, 0., 0.2), None);
    velocity.set_anti_alias(true);
    velocity.set_style(skia_safe::paint::Style::Stroke);
    velocity.set_stroke_width(1.0);

    StateColors { position, velocity }
}

fn target_colors() -> StateColors {
    let mut position =
        skia_safe::Paint::new(skia_safe::Color4f::new(1.0, 1., 0.0, 1.0), None);
    position.set_anti_alias(true);
    position.set_style(skia_safe::paint::Style::Stroke);
    position.set_stroke_width(1.0);

    let mut velocity =
        skia_safe::Paint::new(skia_safe::Color4f::new(1.0, 1.0, 1., 1.0), None);
    velocity.set_anti_alias(true);
    velocity.set_style(skia_safe::paint::Style::Stroke);
    velocity.set_stroke_width(1.0);

    StateColors { position, velocity }
}

fn draw_state(state: &State, canvas: &mut Canvas, colors: StateColors) {
    let direction = na::Vector2::new(state.rotation.re, state.rotation.im);

    // Draw a circle
    canvas.draw_line(
        p(state.position.coords),
        p(state.position.coords + direction * 15.),
        &colors.position,
    );

    // Draw a circle
    canvas.draw_line(
        p(state.position.coords),
        p(state.position.coords + state.velocity),
        &colors.velocity,
    );
}
